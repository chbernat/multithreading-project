package confighandler

type Config struct {
	HOST string `json:"HOST"`
	PORT string `json:"PORT"`
	WaitingTime int `json:"WAITING_TIME_IN_SECONDS""`
	BridgeSettings struct {
		Semaphore int `json:"SEMAPHORE"`
		WesternSide int `json:"WEST"`
		EasternSide int `json:"EAST"`
	} `json:"BRIDGE_SETTINGS"`
}

