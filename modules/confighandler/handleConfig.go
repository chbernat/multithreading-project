package confighandler

import (
	"encoding/json"
	"path"
	"io/ioutil"
	"../utils"
	"sync"
)


var mux sync.Mutex

type ValueSidePair struct {
	Value int
	Entity string
}

func (conf *Config) ReadConfig() []byte {
	configDir := path.Join(utils.GetCurrentWorkingDirectory(), "config.json")
	file, err := ioutil.ReadFile(configDir)
	utils.CheckError(err)
	return file
}

func (conf *Config) WriteToConfig() {
	mux.Lock()
	defer mux.Unlock()
	json := utils.ToJSON(conf)
	err := ioutil.WriteFile("config.json", json, 0644)
	utils.CheckError(err)
}

func (conf *Config) DecodeConfig() {
	configFile := conf.ReadConfig()
	err := json.Unmarshal(configFile, &conf)
	utils.CheckError(err)
}

func (conf *Config) resetValues() {
	conf.BridgeSettings.WesternSide = 0
	conf.BridgeSettings.EasternSide = 0
}

func (conf *Config) HandleOrder() ValueSidePair {
	if(conf.BridgeSettings.WesternSide  > 0 && conf.BridgeSettings.WesternSide > conf.BridgeSettings.EasternSide) {
		conf.BridgeSettings.WesternSide -= conf.BridgeSettings.Semaphore
		return ValueSidePair { Value: conf.BridgeSettings.WesternSide, Entity: "west" }
	} else if (conf.BridgeSettings.EasternSide  > 0 && conf.BridgeSettings.WesternSide < conf.BridgeSettings.EasternSide) {
		conf.BridgeSettings.EasternSide -= conf.BridgeSettings.Semaphore
		return ValueSidePair { Value: conf.BridgeSettings.EasternSide, Entity: "east" }
	}
	conf.resetValues()
	return ValueSidePair { Value: 0, Entity: "both" }
}