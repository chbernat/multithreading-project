package utils

import "os"

func GetCurrentWorkingDirectory () string {
	currentDir, err := os.Getwd()
	CheckError(err)
	return currentDir
}
