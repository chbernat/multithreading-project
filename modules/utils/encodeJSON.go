package utils

import "encoding/json"

func ToJSON(v interface{}) []byte {
	json, err := json.Marshal(v)
	CheckError(err)
	return json
}