import utils from './utilsService';
import config from "../config";
import {WebsocketService} from "./websocketService";

export interface IWebSocketMessage {
    action: IMessageType;
    value: number;
    entity: MessageEntities;
    priority: number;
}

export type MessageEntities = 'west' | 'semaphore' | 'east' | 'app' | 'both';
export type IMessageType = 'set' | 'add' | 'init';
export type IEventsList = { [key: string]: { [key: string]: Function } };
export const MessageTypes: { [key: string]: IMessageType } = {
  'add': 'add',
  'set': 'set',
  'init': 'init'
};

class BridgeWSService extends WebsocketService {

    eventsList: IEventsList = {};

    constructor(websocketAddress: string) {
        super(websocketAddress);
        this.eventsList = {};
        this.websocket.onmessage = this.reactOnEvent.bind(this);
    }

    public onOpen(onOpen: Function) {
        this.websocket.onopen = (onOpen as any);
    }

    public registerEvent(entity: MessageEntities, action: IMessageType, method: Function): void {
        if (utils.isFunction(method) && action && entity) {
            this.checkEntityInList(entity);
            this.eventsList[entity][action] = method;
        }
    }


    private checkEntityInList(entity: MessageEntities): void {
        if (!this.eventsList[entity]) {
           this.eventsList[entity] = {};
        }
    }

    public sendMessage(message: any) {
        this.websocket.send(JSON.stringify(message));
    }

    protected reactOnEvent(event: any): void {
        const message: IWebSocketMessage = JSON.parse(event.data);
        if(message && message.entity && message.action) {
            this.eventsList[message.entity][message.action](message);
        }
    }
}

const bridgeWSService = new BridgeWSService(config.webSocketAddress);

export default bridgeWSService;