export class UtilsService {
    constructor() {}
    isFunction(func: Function): boolean {
        const hasConstructor: boolean = !!(func.constructor);
        const hasCall: boolean = !!(func.call);
        const hasApply: boolean = !!(func.apply);
        return (hasConstructor && hasCall && hasApply);
    }
}

const utils = new UtilsService();

export default utils;
