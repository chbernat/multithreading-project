export abstract class WebsocketService {

    protected websocket: WebSocket;
    protected abstract eventsList: any = {};

    constructor(websocketAddress: string) {
        this.websocket = new WebSocket(websocketAddress);
        this.websocket.onmessage = this.reactOnEvent.bind(this);
    }

    public abstract sendMessage(message: any): void;
    protected abstract registerEvent(...args: any[]): void;
    protected abstract reactOnEvent(message: any): void;

}