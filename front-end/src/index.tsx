import * as React from "react"
import * as ReactDOM from "react-dom"

import { App} from "./components/App"

const exampleElement = document.getElementById("example");

ReactDOM.render(
  <App/>,
  exampleElement
)

// webpack hot module replacement support
if (module.hot) {
  module.hot.accept("./components/App", () => {
    const NextApp= require("./components/App").App;
    ReactDOM.render(
      <NextApp />,
      exampleElement
    )
  })
}
