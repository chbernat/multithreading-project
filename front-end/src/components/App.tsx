import * as React from "react";
// import {BridgeEntity} from "./bridgeEntity/component";
import {EastBridgeSide} from "./bridgeEntity/eastEntity/component";
import {WestBridgeSide} from "./bridgeEntity/westEntity/component";
import {Semaphore} from "./bridgeEntity/semaphoreEntity/component";
import bridgeWSService from "../services/bridgeWSService";
require('./app.scss');

export interface AppProps {}

export class App extends React.Component<AppProps, undefined> {
    constructor() {
        super();
        bridgeWSService.onOpen(this.onOpen.bind(this));
    }

    private onOpen() {
        bridgeWSService.registerEvent('app', 'init', () => console.log("Initialized!"));
        bridgeWSService.sendMessage({
            entity: 'app',
            action: 'init',
            value: 0
        });
    }


    render() {
        return (
            <div className="bridge">
                <EastBridgeSide />
                <Semaphore />
                <WestBridgeSide />
            </div>
        );
    }
}
