
import {BridgeEntity} from "../component";
import {MessageEntities, MessageTypes} from "../../../services/bridgeWSService";

require('./styles.scss');

export class EastBridgeSide extends BridgeEntity {

    entityTitle: string = 'East side of a bridge';
    static entity: MessageEntities = 'east';

    constructor() {
        super(EastBridgeSide.entity);
    }

    addHandler(): void {
        this.sendMessage(EastBridgeSide.entity, MessageTypes.add);
    }

    setHandler(): void {
        this.sendMessage(EastBridgeSide.entity, MessageTypes.set);
    }

}