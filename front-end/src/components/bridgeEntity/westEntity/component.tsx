import {BridgeEntity} from "../component";
import {MessageEntities, MessageTypes} from "../../../services/bridgeWSService";
require('./styles.scss');

export class WestBridgeSide extends BridgeEntity {

    entityTitle: string = 'West side of a bridge';
    static entity: MessageEntities = 'west';

    constructor() {
        super(WestBridgeSide.entity);
    }

    addHandler(): void {
        this.sendMessage(WestBridgeSide.entity, MessageTypes.add);
    }

    setHandler(): void {
        this.sendMessage(WestBridgeSide.entity, MessageTypes.set);
    }

}