import * as React from 'react';
import {
    default as bridgeWSService, IMessageType, IWebSocketMessage, MessageEntities,
    MessageTypes
} from "../../services/bridgeWSService";

require('./styles.scss');

export interface BridgeEntityProps {
    value?: number

}

export interface BridgeEntityState {
    inputValue: string;
    entityValue: string;
    isIndicatorActive: boolean;
}

export abstract class BridgeEntity extends React.Component <BridgeEntityProps, BridgeEntityState> {

    abstract entityTitle: string;
    protected input: HTMLInputElement;

    constructor(entity: MessageEntities) {

        super();

        this.state = ({
            inputValue: '',
            entityValue: '0',
            isIndicatorActive: true,
        } as BridgeEntityState);

        this.registerEvents(entity);
    }

    abstract addHandler(): void;

    abstract setHandler(): void;

    protected registerEvents(entity: MessageEntities) {
        bridgeWSService.registerEvent(entity, MessageTypes.add, this.onAddMessage.bind(this));
        bridgeWSService.registerEvent(entity, MessageTypes.set, this.onSetMessage.bind(this));
        bridgeWSService.registerEvent("both", MessageTypes.set, this.onResetMessage.bind(this));
    }

    private onAddMessage(message: IWebSocketMessage) {
        this.setEntityValue(message.value.toString());
    }
    private toggleIndicator() {
        this.setState({
            ...this.state,
            isIndicatorActive: !this.state.isIndicatorActive
        });
    }
    private onSetMessage(message: IWebSocketMessage) {
        this.setEntityValue(message.value.toString());
    }

    private onResetMessage(message: IWebSocketMessage) {
        this.setEntityValue(message.value.toString());
    }

    protected setEntityValue(entityValue: string) {
        this.setState({...this.state, entityValue});
    }

    protected sendMessage(entity: MessageEntities, action: IMessageType) {
        bridgeWSService.sendMessage({
            action,
            entity,
            value: parseInt(this.state.inputValue, 10)
        });
        this.input.value = '';
    }

    protected updateInputValue(event: React.FormEvent<HTMLInputElement>): void {
        const inputValue = event.currentTarget.value;
        this.setState({...this.state, inputValue});
    }

    render() {
        return (
            <div className="column">
                <h2 className="title">{this.entityTitle}</h2>
                <p id="value">{this.state.entityValue}</p>
                <div className="controls-containers">
                    <input
                        min={0}
                        type="number"
                        id="input"
                        onChange={this.updateInputValue.bind(this)}
                        ref={el => this.input = el}
                        value={this.state.inputValue}
                    />
                    <div className="buttons-container">
                        <a href="#">
                            <button onClick={this.addHandler.bind(this)} className="add" id="add">Add</button>
                        </a>
                        <a href="#">
                            <button onClick={this.setHandler.bind(this)} className="set" id="set">Set</button>
                        </a>
                    </div>
                </div>
                {/*<div className={ `indicator ${this.state.isIndicatorActive ? 'active' : ''}` }/> @TODO */}
            </div>
        );
    }
}