import * as React from "react";
import {BridgeEntity} from "../component";
import {MessageEntities, MessageTypes} from "../../../services/bridgeWSService";

require('./styles.scss');

export class Semaphore extends BridgeEntity {

    entityTitle: string = 'Semaphore';
    input: HTMLInputElement;
    static entity: MessageEntities = 'semaphore';

    constructor() {
        super(Semaphore.entity);
    }

    addHandler(): void {
        this.sendMessage(Semaphore.entity, MessageTypes.add);
    }

    setHandler(): void {
        this.sendMessage(Semaphore.entity, MessageTypes.set);
    }

    render() {
        return (
            <div className="column">
                <h2 className="title">{this.entityTitle}</h2>
                <p id="value">{this.state.entityValue}</p>
                <div className="controls-containers">
                    <input
                        min={0}
                        type="number"
                        id="input"
                        onChange={this.updateInputValue.bind(this)}
                        ref={el => this.input = el}
                        value={this.state.inputValue}
                    />
                    <div className="buttons-container">
                        <a href="#">
                            <button onClick={this.setHandler.bind(this)} className="set" id="set">Set</button>
                        </a>
                    </div>
                </div>
            </div>
        );
    }

}