export interface IConfig {
  webSocketAddress: string;
};

const config: IConfig = {
    webSocketAddress: "ws://localhost:8500/ws"
};

export default config;