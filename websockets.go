package main

import (
	"net/http"
	"github.com/gorilla/websocket"
	"./modules/utils"
	"fmt"
	"time"
)

const WEST string = "west"
const EAST string = "east"
const SEMAPHORE string = "semaphore"

type Message struct {
	Action   string `json:"action"`
	Entity   string `json:"entity"`
	Value    int    `json:"value"`
	Priority int    `json:"priority"`
}

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func Handler(w http.ResponseWriter, r *http.Request) {
	var msg Message
	isWriteable := make(chan bool, 2)
	conn := upgradeUpgrader(w, r)
	go listen(isWriteable, conn, msg)
	go write(isWriteable, conn, msg)
}

func write(isWriteable chan bool, conn *websocket.Conn, msg Message) {
	ticker := time.NewTicker(time.Millisecond * time.Duration(1000 * config.WaitingTime))
	for range ticker.C {
		if (true == <-isWriteable) { // empty a channel and check this value for truth
			isWriteable <- true
			go config.WriteToConfig()
			valueSidePair := config.HandleOrder()
			err := writeMessage(conn, Message{
				Action: "set",
				Entity: valueSidePair.Entity,
				Value:  valueSidePair.Value,
			})
			if err != nil {
				break;
				conn.Close()
			}
		}
	}
}

func listen(isWriteable chan bool, conn *websocket.Conn, msg Message) {
	ticker := time.NewTicker(time.Millisecond)
	for range ticker.C {
		err := conn.ReadJSON(&msg);
		if err != nil {
			fmt.Println("Sender Closing", err)
			break
		}
		switch msg.Entity {
		case "app":
			handleInit(conn)
			break
		case WEST:
			handleWestSide(&msg)
			break
		case SEMAPHORE:
			handleSemaphore(&msg)
			break
		case EAST:
			handleEastSide(&msg)
			break
		default:
			panic("No or wrong entity was specified!")
			return
		}
		isWriteable <- false
		go func() {
			defer func() {
				<-isWriteable // empty a channel
				isWriteable <- true
			}()
			go config.WriteToConfig()
			err := writeMessage(conn, msg)
			if err != nil {
				conn.Close()
				return
			}
		}()
	}
}

func handleInit(conn *websocket.Conn) {

	writeMessage(conn, Message{
		Value:    config.BridgeSettings.EasternSide,
		Entity:   "east",
		Action:   "set",
		Priority: 0,
	})
	writeMessage(conn, Message{
		Value:    config.BridgeSettings.Semaphore,
		Entity:   "semaphore",
		Action:   "set",
		Priority: 0,
	})
	writeMessage(conn, Message{
		Value:    config.BridgeSettings.WesternSide,
		Entity:   "west",
		Action:   "set",
		Priority: 0,
	})
}

func upgradeUpgrader(w http.ResponseWriter, r *http.Request) *websocket.Conn {
	conn, err := upgrader.Upgrade(w, r, nil)
	utils.CheckError(err)
	return conn
}

func writeMessage(conn *websocket.Conn, msg interface{}) error {
	err := conn.WriteJSON(msg)
	return err
}

// @TODO simplify functions handling sides

func handleEastSide(message *Message) {
	if (message.Action == "add") {
		config.BridgeSettings.EasternSide += message.Value
	} else {
		config.BridgeSettings.EasternSide = message.Value
	}
	message.Value = config.BridgeSettings.EasternSide
}

func handleWestSide(message *Message) {
	if message.Action == "add" {
		config.BridgeSettings.WesternSide = config.BridgeSettings.WesternSide + message.Value
	} else {
		config.BridgeSettings.WesternSide = message.Value
	}
	message.Value = config.BridgeSettings.WesternSide
}

func handleSemaphore(message *Message) {
	if message.Action == "add" {
		config.BridgeSettings.Semaphore += message.Value
	} else {
		config.BridgeSettings.Semaphore = message.Value
	}
	message.Value = config.BridgeSettings.Semaphore
}
