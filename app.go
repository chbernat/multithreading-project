package main

import (
	"./modules/confighandler"
	"fmt"
	"net/http"
)

var config confighandler.Config

func main() {
	config.DecodeConfig()
	fmt.Printf("%v", config)
	http.HandleFunc("/ws", Handler)
	http.ListenAndServe(config.PORT, nil)
}
